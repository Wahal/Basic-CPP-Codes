#include<iostream>

using namespace std;

class A {
protected:
    int a;
public:
    A () { a = 10;}
};

class B {
protected:
  int b;
public:
  B() { b = 20;}
};

class C: public A, public B {
public:
  void fun();
  int divide () {
    int x = 5, y = 0;
    try {
      if (y == 0){
        throw 0;
      }
    return (x/y);
  }
  catch (int er) {
    cout << "WTF Man !";
  }
  }

};

void C::fun ()
{
  cout << "a = " << A::a << "\n";
  cout << "b = " << B::b << "\n";
}

int main (){
  C first;
  //first.fun();
  cout << first.divide();
  return 0;
}
