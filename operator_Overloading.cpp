#include<iostream>

using namespace std;

class Douche {

    int age;
  public:

    Douche () {
        age = 0;
    }
    Douche (int entered_age) {
        age = entered_age;
    }
    display () {
      cout << "Age of Douche: " << age << endl;
    }

    Douche operator + (Douche d) {
      Douche temporary;
      temporary.age = age + d.age;
      return temporary;
    }
};

int main() {

    Douche one(18), two(20), three(one + two);
    cout << one.display() << endl << two.display();
    cout << three.display() << endl;
    return 0;
}
